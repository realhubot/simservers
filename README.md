# simserveRs #

This project implement a flexible and easily configurable simulator for a system of servers in parallel with FCFS (first come, first served) queueing discipline.

In particular, we use the simulator for implementing some experiments. At present, we provide the calculation of the mean first-passage time (MFPT) to the critical condition of the system, that is, all the servers are busy and the next incoming call goes to the waiting queue. Also, we compute the residence time spent by the ongoing system in each state.

For more details, please see https://doi.org/10.3390/mca26040070

### Description of simserveRs function ###

**simserveRs(ULL,servers,ftss,queue)**: generates an incoming call and updates the state of the system (servers and queue) following the flowchart:

![Alt text](simserveRs.v2.png)

The rectangles in the flowchart are basic functions defined within simserveRs.R:

**callin(ULL)**: generate a new call at ULL with exponential distribution of lapses between calls

**serverin(ULL, UE, servers,ftss)**: refresh the state of a server when it is assigned to an incoming call or one in queue.
The service time can be drawn either from a lognormal or exponential distribution

**serverout(ULL,servers)**: release of busy servers with US < ULL

**queuein(queue,ULL)**: puts the last call at the bottom of the queue

**queueout(ULL,queue,servers,ftss)**: under saturation, puts iteratively in service the older calls whereas USmin < ULL.
Then, if the queue is empty, releases all servers with USmin < ULL


### Description of simcritical function ###

**simcritical()**: performs a single experiment of first-passage time to the critical condition (first call passed to queue).
It defines and initializes the state of system and records the first-passage time to the queue from each possible stress state (ftss).


### Description of mfpt function ###

**mfpt(NSV,Tc,Ts)**: performs the analytical calculation of MFPT to queuing from each stress state under the assumption that distributions of time between calls and service times are both exponential with
*Tc = mean inter-arrival time* and *Ts = mean service time* (see https://arxiv.org/abs/1602.06579).

### Description of mfpt-simulator ##

This code implement a full simulation for calculating MFPT to critical condition with its corresponding standard deviation.
To run an experiment, just invoke:

     $ Rscript mfpt-simulator.R NSV Nsim NMTc NMTs NSTs MTc.min MTc.max MTs.min MTs.max STs.min STs.max srv

where the simulation parameters are

Parameter | value
:-------- | :--------------------------------------------------
NSV       | Number of servers
Nsim      | Number of simulations
NMTc      | Number of points in the interval [MTc.min, MTc.max]
NMTs      | Number of points in the interval [MTs.min, MTs.max]
NSTs      | Number of points in the interval [STs.min, STs.max]
MTc.min   | Minimum value for mean inter-arrival time
MTc.max   | Maximun value for mean inter-arrival time
MTs.min   | Minimum value for meanlog service time
MTs.max   | Maximun value for meanlog service time
STs.min   | Minimum value for sdlog service time
STs.max   | Maximun value for sdlog service time
srv       | "exp" or "lognormal" (*)

(*) set probability distribution of service times.
The implemented options are exponential and lognormal, but other distributions can be easily defined through the service() function within the simserveRs code.

The parallel version requires:

     > library("parallel")

### Description of ssdistro function ###

**ssdistro(NSV,Tc,Ts)**: performs the calculation of the analytical distribution for the steady state of a Markov chain with a reflecting boundary at origin.

### Description of simresidence ##

This code implement a simulation for computing the distribution of time spent by an ongoing system in each state.
To run an experiment, just invoke:

     $ Rscript simresidence.R tss NSV MTc MTs STs srv

where the simulation parameters are

Parameter | value
:-------- | :-----------------------
tss       | time span simulated
NSV       | Number of servers
MTc       | Mean inter-arrival time
MTs       | Meanlog service time
STs       | Sdlog service time
srv       | "exp" or "lognormal"

### Citation

A paper describing simserveRs has been published. Please cite this project as:
Time to Critical Condition in Emergency Services, Pedro A. Pury, Math. Comput. Appl. 2021, **26**(4), 70 (2021); DOI: [10.3390/mca26040070](https://doi.org/10.3390/mca26040070)

#### Disclaimer ####

simserveRs is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#### License ####

Software distributed under license GPL-V3. See full text in [`LICENSE`](https://bitbucket.org/realhubot/simservers/src/master/LICENSE).

---
#### Contact ####

[Pedro Pury](http://www.famaf.unc.edu.ar/~pury/)
FaMAF-UNC (C) 2017-20