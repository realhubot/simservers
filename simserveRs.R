##*******************************************************************
##*******************************************************************
##*****  simserveRs
##*****  Pedro Pury FaMAF-UNC (C)2017-2018
##*****  Ver.0.1 - September 18, 2017 -> fork from simURG
##*****  Ver.0.2 - June 08, 2018      -> simserveRs as function
##*****  Ver.0.3 - August 25, 2020    -> more explicit logic in MAIN
##*****                            and simplifying queueout function
##*****  Ver.0.4 - August 27, 2020    -> queueoutR function
##*******************************************************************
##*******************************************************************
## Servers in parallel with one queue and FCFS discipline
## (first come, first served)
#
## simserveRs generates a new call and updates the state
## of the system (servers and queue)
# *******************************************************************
#
## simserveRs is a function that must be called with
## the following variables defined and initialized
# #
# # Lcall: instant of last incoming call (set the origin of time)
#      Lcall = 0.
# #
# # queue: Calls in waiting list for service under saturation
#      queue <- data.frame(matrix(ncol=1, nrow=0))
#      colnames(queue) <- "Tcalls"
# #
# # servers(i,j): matrix of servers states
# #       i = server labels (1, ..., NSV)
# #       j = 1 : occupation state (logical)
# #           2 : load:  number of services performed
# #           3 : Tcall: time of incoming call (0: free)
# #           4 : UE time of occupation        (0: free)
# #           5 : US time of release           (0: free)
# #
# # Initial state of servers
#      attrb   <- as.integer(5)
#      state   <- array(FALSE, dim=c(NSV,1))
#      initial <- array(0, dim=c(NSV,attrb-1))
#      servers <- data.frame(state,initial)
#      colnames(servers) <- c("state","load","Tcall","UE","US")
# #
# # First two attributes are preserved in the release of a server
#      assign("attpr", value = 2, envir = .GlobalEnv)
#
# # Record of first-passage time to a new value of stress
# # from initial state of zero stress
#      first_time <- array(0, dim=c(NSV+1,1))
#      state   <- array(FALSE, dim=c(NSV+1,1))
#      ftss <- data.frame(state,first_time)
#      rownames(ftss) <- c(0:NSV)
#      ftss["0",] <- c(TRUE,0.)
#
##*******************************************************************
##*******************************************************************
## MAIN simserveRs (see flowchart)
#
     simserveRs <- function(Lcall,servers,ftss,queue) {
#
          Lcall <- callin(Lcall)
#
          if (stress(servers)==0) {
               sf.list <- serverin(Lcall,Lcall,servers,ftss)
               servers <- sf.list[[1]]
               ftss    <- sf.list[[2]]
          }
          else {
               if (Lcall < minUS(servers)) {
                    if (saturation(servers)) {
                         queue <- queuein(queue,Lcall)
                    }
                    else {
                         sf.list <- serverin(Lcall,Lcall,servers,ftss)
                         servers <- sf.list[[1]]
                         ftss    <- sf.list[[2]]
                    }
               }
               else{
                    if (nrow(queue) == 0) {
                         servers <- serversout(Lcall,servers)
                         sf.list <- serverin(Lcall,Lcall,servers,ftss)
                         servers <- sf.list[[1]]
                         ftss    <- sf.list[[2]]
                    }
                    else {
                         qs.list <- queueout(Lcall,queue,servers,ftss)
                         queue   <- qs.list[[1]]
                         servers <- qs.list[[2]]
                         ftss    <- qs.list[[3]]
                         if (Lcall < minUS(servers)) {
                              queue <- queuein(queue,Lcall)
                         }
                         else {
                              servers <- serversout(Lcall,servers)
                              sf.list <- serverin(Lcall,Lcall,servers,ftss)
                              servers <- sf.list[[1]]
                              ftss    <- sf.list[[2]]
                         }
                    }
               }
          }
     return(list(Lcall,servers,ftss,queue)) }
#
#    For calling the function use:
#    srv.list <- simserveRs(Lcall,servers,ftss,queue)
#    Lcall   <- srv.list[[1]]
#    servers <- srv.list[[2]]
#    ftss    <- srv.list[[3]]
#    queue   <- srv.list[[4]]
#
# *******************************************************************
# *******************************************************************
## Functions
#
# *******************************************************************
# callin ->     Calling times generaror
#               Exponential distribucion for lapse between calls
#               MTc must be in global enviroment
#
     callin <- function(Lcall) {
          ULT <- Lcall + rexp(1, rate = 1./MTc)
     return(ULT) }
#
# *******************************************************************
# service ->    Service times generator
#               Lognormal or Exponential distribution
#               srv, MTs and STs must be in global enviroment
#
      service <- function() {
          if (srv == "exp") {
               MeanTs <- exp(MTs + STs^2 / 2)
               US <- rexp(1, rate = 1./MeanTs)
          }
          else {
               US <- rlnorm(1, meanlog = MTs, sdlog = STs)
          }
      return(US) }
#
# *******************************************************************
# stress ->     Number of busy servers
#
     stress  <- function(servers) {
          states <- servers[,"state"]
          NSV <- length(states)
          nbusy <- as.integer(0)
          for (ind in 1:NSV) {
               if (states[ind]) {nbusy <- as.integer(nbusy+1)}
          }
     return(nbusy) }
#
# *******************************************************************
# saturation -> Full occupation of servers  (return logical)
#
     saturation <- function(servers) {
          full <- FALSE
          NSV <- length(servers[,"state"])
          if (stress(servers) == NSV) {full <- TRUE}
     return(full) }
#
# *******************************************************************
# dispatch ->   Perfoms a ramdom permutation
#               and assigns the first unit not busy
#               The function reports error if it called
#               under saturation and returns NULL
#
     dispatch <- function(servers) {
          if (saturation(servers)) {
               print("ERROR dispatch -> Saturation",quote=FALSE)
               unit <- NULL
          }
          else {
               NSV <- length(servers[,"state"])
               units <- c(1:NSV)
               runits <- sample(units, NSV, replace=FALSE, prob=NULL)
               state <- TRUE
               ind <- as.integer(1)
               while (state) {
                    unit <- runits[ind]
                    state <- servers[unit,"state"]
                    ind   <- as.integer(ind+1)
               }
          }
     return(unit) }
#
# *******************************************************************
# serverin ->   Refresh the state of a server when it is assigned
#               to an incoming call or one in queue
#               The function reports error if it called
#               under saturation
#
     serverin <- function(Lcall, UE, servers,ftss) {
          if (saturation(servers)) {
               print("ERROR serverin -> Saturation", quote=FALSE)
          }
          else {
               unit <- dispatch(servers)
               US <- UE + service()
               ST <- servers[unit,"load"] + as.integer(1)
               servers[unit,] <- data.frame(TRUE,ST,Lcall,UE,US)
               if (debug) { print(servers[unit,]) }
               #
               # Save the first-time stress reachs a new value
               ss  <- stress(servers)
               css <- as.character(ss)
               if (debug) { print(paste("stress = ",ss), quote=FALSE) }
               if (ftss[css,"state"] == FALSE) {
                    ftss[css,"first_time"] <- UE
                    ftss[css,"state"] <- TRUE
               }
          }
     return(list(servers,ftss)) }
#
#    For calling the function use:
#    sf.list <- serverin(servers,ftss)
#    servers <- sf.list[[1]]
#    ftss    <- sf.list[[2]]
#
# *******************************************************************
# minUS ->      Minimum non-null release time of servers
#               The function reports error if it called
#               without stress
#
     minUS <- function(servers) {
          if (stress(servers) == 0) {
               print("ERROR minUS -> stress=0", quote=FALSE)
          }
          else {
               uss <- servers[,"US"]
               NSV <- length(uss)
               # begin with the maximun time
               minus <- uss[do.call(order, -as.data.frame(uss))][1]
               # search for the minimum non-null time in the list
               for (ind in 1:NSV) {
                    if (uss[ind]!=0 & minus > uss[ind]) {
                        minus <- uss[ind]
                    }
               }
          }
     return(minus) }
#
# *******************************************************************
# release ->    Release of busy server
#               The function reports error
#               if it called for a free server
#               attpr must be in global enviroment
#
     release <- function(unit,servers) {
          if (!servers[unit,"state"]) {
               print("ERROR release -> free server", quote=FALSE)
          }
          else {
               servers[unit,"state"] <- FALSE
               if (debug) { print(servers[unit,]) }
               # First attpr attributes are preserved
               attini <- attpr + as.integer(1)
               attrb <- length(colnames(servers))
               servers[unit,attini:attrb] <- 0
          }
     return(servers)}
#
# *******************************************************************
# serversout -> Release of busy servers with US < UU
#               The function reports error if it called
#               without stress or US >= UU
#
     serversout <- function(UU,servers) {
          if (stress(servers) == 0) {
               print("ERROR serversout -> stress=0", quote=FALSE)
          }
          else {
               if (UU < minUS(servers)) {
                    print("ERROR serversout -> UU<minUS", quote=FALSE)
               }
               else {
                    usmin <- minUS(servers)
                    while (UU >= usmin & stress(servers) != 0) {
                         L <- servers$US == usmin
                         unit <- row.names(servers[L,])
                         servers <- release(unit,servers)
                         if (stress(servers) != 0) {
                              usmin <- minUS(servers)
                         }
                    }
               }
          }
     return(servers)}
#
# *******************************************************************
# queuein ->    Puts the last call in the botton of the queue
#
     queuein <- function(queue,Lcall) {
          fila <- nrow(queue) + 1
          queue[fila,]  <- Lcall
          if (debug) { print(queue) }
     return(queue) }
#
# *******************************************************************
# queueclean -> Cleaning of the older call in queue
#
     queueclean <- function(queue) {
          if (nrow(queue) == 1) {
               # creates a void queue
               queue <- data.frame(matrix(ncol=1, nrow=0))
               colnames(queue) <- "Tcalls"
          }
          else {
               # delete de older call in the queue
               queue <- data.frame(queue[2:nrow(queue),])
               colnames(queue) <- "Tcalls"
          }
     return(queue) }
#
# *******************************************************************
# queueout ->   Under saturation, puts iteratively in service
#               the older calls in queue whereas USmin < Lcall
#               The function reports error if it called without queue
#
     queueout <- function(Lcall,queue,servers,ftss) {
          if (nrow(queue) == 0) {
               print("ERROR queueout -> queue=NULL", quote=FALSE)
          }
          else {
               usmin <- minUS(servers)
               while (usmin < Lcall & nrow(queue) != 0){
                    # release of server with USmin
                    L <- servers$US == usmin
                    unit <- row.names(servers[L,])
                    servers <- release(unit,servers)
                    # call in queue is assigned to the server
                    sf.list <- serverin(queue[1,], usmin, servers,ftss)
                    servers <- sf.list[[1]]
                    ftss    <- sf.list[[2]]
                    usmin <- minUS(servers)
                    # cleaning the queue
                    queue <- queueclean(queue)
                    if (debug) { print(queue) }
               }
#                Then, if the queue is empty, releases
#                all servers with USmin < Lcall
#                if (usmin < Lcall & nrow(queue) == 0) {
#                     servers <- serversout(Lcall,servers)
#                }
          }
     return(list(queue,servers,ftss)) }
#
#    For calling the function use:
#    qs.list <- queueout(Lcall,queue,servers,ftss)
#    queue   <- qs.list[[1]]
#    servers <- qs.list[[2]]
#    ftss    <- qs.list[[3]]
#
# *******************************************************************
# queueoutR ->  Under saturation, puts iteratively in service
#               the older calls in queue whereas USmin < Lcall
#               Provides the time of last exit from queue = lastq
#               The function reports error if it called without queue
#
     queueoutR <- function(Lcall,queue,servers,ftss) {
          if (nrow(queue) == 0) {
               print("ERROR queueout -> queue=NULL", quote=FALSE)
          }
          else {
               usmin <- minUS(servers)
               while (usmin < Lcall & nrow(queue) != 0){
                    lastq <- usmin
                    # release of server with USmin
                    L <- servers$US == usmin
                    unit <- row.names(servers[L,])
                    servers <- release(unit,servers)
                    # call in queue is assigned to the server
                    sf.list <- serverin(queue[1,], usmin, servers,ftss)
                    servers <- sf.list[[1]]
                    ftss    <- sf.list[[2]]
                    usmin <- minUS(servers)
                    # cleaning the queue
                    queue <- queueclean(queue)
                    if (debug) { print(queue) }
               }
          }
     return(list(queue,servers,ftss,lastq)) }
#
#    For calling the function use:
#    qs.list <- queueout(Lcall,queue,servers,ftss)
#    queue   <- qs.list[[1]]
#    servers <- qs.list[[2]]
#    ftss    <- qs.list[[3]]
#    lastq   <- qs.list[[4]]
#
# *******************************************************************
# *******************************************************************