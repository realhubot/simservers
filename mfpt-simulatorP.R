##*******************************************************************
##*******************************************************************
##*****  MFPT
##*****  Pedro Pury FaMAF-UNC (C)2017
##*****  Ver.0.1 - October   10, 2017
##*****  Ver.0.2 - October   16, 2017 (loop in MTc)
##*****  Ver.0.3 - November  02, 2017 (average over initial states)
##*****  Ver.0.4 - July      18, 2019 (loops in MTs and STs)
##*****  Ver.0.5 - July      21, 2019 (parallel simulator)
##*******************************************************************
##    Mean first-passage time to queuing from each value of stress
##    for a system of servers in parallel and FCFS discipline
##*******************************************************************
#
     library("parallel")
#
# Comand line from UNIX shell
# > Rscript mfpt-simulatorP.R NSV Nsim Ncrs NMTc NMTs NSTs MTc.min MTc.max MTs.min MTs.max STs.min STs.max srv
#
# where the argumentas are
# NSV       | Number of servers
# Nsim      | Number of simulations
# Ncrs      | number of cores for parallelization
# NMTc      | Number of points in the interval [MTc.min, MTc.max]
# NMTs      | Number of points in the interval [MTs.min, MTs.max]
# NSTs      | Number of points in the interval [STs.min, STs.max]
# MTc.min   | Minimum value for mean inter-arrival time
# MTc.max   | Maximun value for mean inter-arrival time
# MTs.min   | Minimum value for meanlog service time
# MTs.max   | Maximun value for meanlog service time
# STs.min   | Minimum value for sdlog service time
# STs.max   | Maximun value for sdlog service time
# srv       | Probability density of service times:
#           | "exp" or "lognormal"
#
# Example: Rscript mfpt-simulatorP.R 7 2 1 11 11 11 5.0 15.0 3.0 4.0 0.2 0.6 "lognormal"
#
# *******************************************************************
# Setting the seed of random number generator (Mersenne-Twister)
# (if we need reproduce simulacions)
#     set.seed(483246841, kind = "default", normal.kind = "default")
#
# *******************************************************************
## Simulation parameters (global)
#
# Simulation parameters from command line
#
     args <- commandArgs(TRUE)
     NSV     <- as.integer(args[1])
     Nsim    <- as.integer(args[2])
     Ncrs    <- as.integer(args[3])
     NMTc    <- as.integer(args[4])
     NMTs    <- as.integer(args[5])
     NSTs    <- as.integer(args[6])
     MTc.min <- as.numeric(args[7])
     MTc.max <- as.numeric(args[8])
     MTs.min <- as.numeric(args[9])
     MTs.max <- as.numeric(args[10])
     STs.min <- as.numeric(args[11])
     STs.max <- as.numeric(args[12])
     srv     <- as.character(args[13])
#
# Steps for parameter's intervals
#
     if (NMTc < 2) {NMTc <- 2}
     if (NMTs < 2) {NMTs <- 2}
     if (NSTs < 2) {NSTs <- 2}
#
     delta.MTc <- (MTc.max-MTc.min)/(NMTc-1)
     delta.MTs <- (MTs.max-MTs.min)/(NMTs-1)
     delta.STs <- (STs.max-STs.min)/(NSTs-1)
#
# Names of writing files
#
     cnsv <- as.character(NSV)
     filew1 <- paste0("mfpt",cnsv,".csv")
     filew2 <- paste0("avmfpt",cnsv,".csv")
     filew3 <- paste0("times",cnsv,".csv")
#
     exitdata <- c("MTc","MTs","STs","srv","time")
     write(exitdata, file=filew3, ncolumns = 5, append = TRUE, sep = "," )
#
# *******************************************************************
# *******************************************************************
## Functions
# *******************************************************************
# *******************************************************************
# simcritical provides FPT to queue from each state of stress
#
     source("simcritical.R")
#
# *******************************************************************
# mfpt provide the analytical formula for MFPT
# from a given state of stress to queuing
#
     source("mfpt.R")
#
# *******************************************************************
# avevar ->     Upgrade the average and variance of a set
#               of ndat data when a new datum is added
#               If newdat is the first datum, ndat must be zero !
#
     avevar <- function(ndat,average,variance,newdat) {
          if (ndat == 0) {
               average  <- newdat
               variance <- 0.
          }
          else {
               avernew  <- average + (newdat - average) / (ndat+1)
               variance <- {(ndat-1) / (ndat) * variance} +
                           {(ndat+1) * (avernew - average)^2}
               average  <- avernew
          }
     return(list(average,variance)) }
#
# *******************************************************************
# aveini ->     Average over initial states
#
     aveini <- function(vector,L,Tc,Ts) {
          P <- array(0, dim=c(L+1,1))
          P <- as.data.frame(P)
          rownames(P) <- 0:L
          colnames(P) <- "probabilidad"
          # traffic intensity
          rho <- Ts / (L*Tc)
          # normalization
          S <- as.numeric(0)
          for (i in 0:L) {
               S <- S + (L*rho)^i / factorial(i)
          }
          # probalitities
          for (j in 0:L) {
               cj <- as.character(j)
               P[cj,] <- (L*rho)^j / factorial(j) / S
          }
          # average
          average <- as.numeric(0)
          for (k in 0:L) {
               ck <- as.character(k)
               average <- average + P[ck,] * vector[ck,]
          }
     return(average) }
#
# *******************************************************************
# *******************************************************************
# Definition of data.frames
#
     MFPT <- array(0, dim=c(NSV+1,4))
     MFPT <- as.data.frame(MFPT)
     rownames(MFPT) <- c(0:NSV)
     colnames(MFPT) <- c("analytic","mfpt","sampleSD","stadev")
     #
     mfpt.var <- array(0, dim=c(NSV+1,2))
     mfpt.var <- data.frame(mfpt.var)
     rownames(mfpt.var) <- c(0:NSV)
     colnames(mfpt.var) <- c("mfpt","variance")
#
# *******************************************************************
# MAIN loops for MTs, STs, and MTc
#
     for (MTc in seq(MTc.min, MTc.max, delta.MTc)) {
     for (MTs in seq(MTs.min, MTs.max, delta.MTs)) {
     for (STs in seq(STs.min, STs.max, delta.STs)) {
#
# Mean service time from lognormal parameters
          Ts <- exp(MTs + STs^2 / 2)
#
# *******************************************************************
# Loop of simulation
#
          fpt <- list
          time <- system.time(
          fpt  <- mclapply(1:Nsim, function(ind) {
               simcritical()
          },mc.cores=Ncrs)
#           fpt  <- lapply(1:Nsim, function(ind) {
#                simcritical()
#           })
          ,gcFirst = TRUE)
#
# Average and variance
#
          for (ind in 1:Nsim) {
               for (k in 0: NSV) {
                    ck <- as.character(k)
                    average  <- mfpt.var[ck,"mfpt"]
                    variance <- mfpt.var[ck,"variance"]
                    newdat   <- fpt[[ind]][ck,"first_time"]
                    av.list <- avevar(ind-1,average,variance,newdat)
                    mfpt.var[ck,"mfpt"]     <- av.list[[1]]
                    mfpt.var[ck,"variance"] <- av.list[[2]]
               }
          }
#
# *******************************************************************
# Simulations
#
          MFPT["mfpt"]     <- mfpt.var["mfpt"]
          MFPT["sampleSD"] <- sqrt(mfpt.var["variance"])
          MFPT["stadev"]   <- MFPT["sampleSD"] / sqrt(Nsim)
#
# Formula
#
          MFPT["analytic"] <- mfpt(NSV,MTc,Ts)
#
# *******************************************************************
# Writing down to files
#
          exitdata1 <- as.vector(t(MFPT["analytic"]))
          exitdata2 <- as.vector(t(MFPT["mfpt"]))
          exitdata3 <- as.vector(t(MFPT["stadev"]))
          exitdata  <- c(MTc,MTs,STs,exitdata1,exitdata2,exitdata3)
#
          write.table(t(exitdata), file=filew1,
               row.names = FALSE, col.names = FALSE, append = TRUE,
               sep = ",")
#
          exitdata4 <- aveini(MFPT["analytic"],NSV,MTc,Ts)
          exitdata5 <- aveini(MFPT["mfpt"],NSV,MTc,Ts)
          exitdata  <- c(MTc,MTs,STs,exitdata4,exitdata5)
#
          write(exitdata, file=filew2, append = TRUE, sep = ",")
#
          exitdata  <- c(MTc,MTs,STs,srv,time[[3]])
          write(exitdata, file=filew3, ncolumns = 5, append = TRUE, sep = ",")
#
     }}}
#
#      print(c("MTc =",MTc,"MTs =",MTs,"STs =",STs,"Ts =",Ts,"time =",time[[3]]), quote=FALSE)
#      print(MFPT)
#
##*******************************************************************
##*******************************************************************