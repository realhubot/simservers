##*******************************************************************
##*******************************************************************
##*****  MFPT
##*****  Pedro Pury FaMAF-UNC (C)2017
##*****  Ver.0.1 - October 10, 2017
##*******************************************************************
##*******************************************************************
##  Analytical MFPT to queuing from a given stress state
##  for a system with L servers in parallel (FCFS discipline)
##  See https://arxiv.org/abs/1602.06579
##*******************************************************************
##*******************************************************************
#
# *******************************************************************
# *******************************************************************
# sums -> Evaluation of double summations in mfpt
#
     sums <- function(m,g) {
          sumext <- 0.
          for (k in 0:m) {
               sumint <- 0.
               for (i in (k+1):(m+1)) {
                    sumint <- sumint + factorial(i) * g^i
               }
               sumext <- sumext + sumint / (g^k * factorial(k))
          }
          ssums <- sumext
     return(ssums) }
#
# *******************************************************************
# *******************************************************************
# mfpt -> Analytical calculation of MFPT to queuing
#         from a given stress state
#         Assumptions: Distributions of time betwwen
#         calls and services time are both exponentials
#         L  = number of servers
#         Tc = mean time betwwen calls
#         Ts = mean services time
#
     mfpt <- function(L,Tc,Ts) {
          #
          T <- array(0, dim=c(L+1,1))
          T <- data.frame(T)
          colnames(T) <- c("mfpt")
          rownames(T) <- c(0:L)
          #
          gamma <- Tc / Ts
#          print(gamma)
          #
          if (L == 0) {
               T["0","mfpt"] <- Tc
          }
          else {
               T["0","mfpt"] <- Tc * (L+1 + sums(L-1,gamma))
               T["1","mfpt"] <- T["0","mfpt"] - Tc
               if (L > 1) {
                    for (ind in 2:L) {
                         cind <- as.character(ind)
                         aux <- Tc * (ind + sums(ind-2,gamma))
                         T[cind,"mfpt"] <- T["0","mfpt"] - aux
                    }
               }
          }
     return(T) }
#
# *******************************************************************
# *******************************************************************