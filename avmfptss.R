##*******************************************************************
##*******************************************************************
##*****  avmfptss
##*****  Pedro Pury FaMAF-UNC (C)2020
##*****  Ver.0.1 - September 29, 2020
##*******************************************************************
##*******************************************************************
##  avmfptss computes the average MFPT and steady state
##  probability of find the system free
##*******************************************************************
##*******************************************************************
#
# Comand line from UNIX shell
# > Rscript avmfptss.R NSV NTc Tc.min Tc.max Ts
#
# where the argumentas are
# NSV    | Number of servers
# NTc    | Number of points in the interval [MTc.min, MTc.max]
# Tc.min | Minimum value for mean inter-arrival time
# Tc.max | Maximun value for mean inter-arrival time
# Ts     | Mean service time
#
# Example: Rscript avmfptss.R 7 200 8 15 44.1
#
# *******************************************************************
## Parameters
#
# Parameters from command line
#
     args <- commandArgs(TRUE)
     NSV    <- as.integer(args[1])
     NTc    <- as.integer(args[2])
     Tc.min <- as.numeric(args[3])
     Tc.max <- as.numeric(args[4])
     Ts     <- as.numeric(args[5])
#
     if (NTc < 2) {NTc <- 2}
     delta.Tc <- (Tc.max-Tc.min)/(NTc-1)
#
# Name of writing files
#
     cnsv  <- as.character(NSV)
     file  <- paste0("avmfptss",cnsv)
     filew <- paste0(file,".csv")
     fileg <- paste0(file,".pdf")
#
# *******************************************************************
# *******************************************************************
## Functions
# *******************************************************************
# MFPT provides the analytical MFPT to queuing from a given stress
# for a state birth-death process with a reflecting boundary at origin
#
     source("mfpt.R")
#
# ssdistro provides the analytical probability distribution for the
# steady state of a Markov chain with a reflecting boundary at origin
#
     source("ssdistro.R")
#
# *******************************************************************
#
     output <- array(0, dim=c(NTc,3))
     output <- as.data.frame(output)
     colnames(output) <- c("Tc","Pfree","avMFPT")
#
# *******************************************************************
## MAIN avmfptss
#
     counter <- as.integer(0)
     for (Tc in seq(Tc.min, Tc.max, delta.Tc)) {
          counter <- counter + 1
          MFPT <- mfpt(NSV,Tc,Ts)
          SSDISTRO <- ssdistro(NSV,Tc,Ts)
          full <- as.character(NSV)
#         Probability of finding the system free in stedy state regime
          Pfree <- 1 - SSDISTRO[full,"probability"]
          Pfree <- Pfree - SSDISTRO["q","probability"]
#         Renormalization of probabilities on [0,NSV]
          Pss <- SSDISTRO["probability"] / SSDISTRO["0","probability"]
          S <- sum(Pss["probability"]) - Pss["q","probability"]
          Pss <- Pss / S
#         Average MFPT
          avmfpt <- as.numeric(0)
          for (k in 0:NSV) {
               ck <- as.character(k)
               avmfpt <- avmfpt + Pss[ck,"probability"] * MFPT[ck,"mfpt"]
          }
     output[counter,"Tc"] <- Tc
     output[counter,"Pfree"]  <- Pfree
     output[counter,"avMFPT"] <- avmfpt
     }
#
     write.table(output, file=filew,
          row.names = FALSE, col.names = TRUE,
          sep = ",")
#
# *******************************************************************
#
     library(ggplot2)
#
     D <- ggplot(data = output, aes(x = Pfree, y = avMFPT)) +
               geom_line(colour = "blue") + theme_minimal()
     pdf(fileg,width=5,height=5,paper='special')
     D
     dev.off()
#
##*******************************************************************
##*******************************************************************